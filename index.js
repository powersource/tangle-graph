const { LinkMap, BacklinkMap } = require('./maps')
const Lookup = require('./lookup')
const isRoot = require('./lib/is-root')

module.exports = class Graph {
  constructor (nodes) {
    this.nodes = nodes
    this.setupGraph()
  }

  setupGraph () {
    if (!Array.isArray(this.nodes)) error('nodes to be an Array')
    if (new Set(this.nodes.map(node => node.key)).size !== this.nodes.length) {
      error('each node to have a unique key')
    }

    this.rootKeys = this.nodes
      .filter(node => isRoot(node))
      .map(node => node.key)
    if (this.nodes.length && this.rootKeys.length === 0) error('at least one root node')

    if (containsSelfLoop(this.nodes)) throw new Error('there is a selfloop in nodes')

    // build map of each hop which runs forward causally
    this.linkMap = new LinkMap(this.nodes)
    // and the inverse
    this.backlinkMap = new BacklinkMap(this.linkMap)
    this.lookup = new Lookup({ nodes: this.nodes, linkMap: this.linkMap })

    // if (containsCycles(this.lookup.connected, this._getBacklinks)) throw new Error('there is a cycle in connected nodes')
  }

  getNode (key) {
    return this.lookup.getNode(key)
  }

  getNext (key) {
    if (!this.lookup.isConnected(key)) return []

    return this.linkMap.get(key)
  }

  // alias
  getLinks (key) {
    return this.getNext(key)
  }

  getPrevious (key) {
    if (!this.lookup.isConnected(key)) return []
    return this.backlinkMap.get(key)
  }

  // alias
  getBacklinks (key) {
    return this.getPrevious(key)
  }

  isBranchNode (key) {
    if (!this.lookup.isConnected(key)) return false
    return this.linkMap.get(key)
      .filter(link => this.lookup.isConnected(link))
      .length > 1
  }

  isMergeNode (key) {
    if (!this.lookup.isConnected(key)) return false
    return this.backlinkMap.get(key).length > 1
  }

  isTipNode (key) {
    if (!this.lookup.isConnected(key)) return false

    return this.linkMap.get(key)
      .filter(link => this.lookup.isConnected(link))
      .length === 0
  }

  invalidateKeys (keys) {
    const prunedKeys = this.linkMap.remove(keys)
    this.backlinkMap = new BacklinkMap(this.linkMap)

    this.lookup.disconnect(prunedKeys)
  }

  get rootNodeKeys () { return this.rootKeys }
  get rootNodes () { return this.rootKeys.map(k => this.getNode(k)) }

  get raw () {
    return {
      linkMap: this.linkMap.raw,
      backlinkMap: this.backlinkMap.raw
    }
  }

  addNodes (newNodes) {
    const undoNodes = this.nodes // This will revert changes to the nodes, but not to linkmaps or anything.
    // Modifying graph
    this.nodes = [...this.nodes, ...newNodes]

    try {
      this.setupGraph()
    } catch (err) {
      this.nodes = undoNodes
      throw err
    }
  }

  getHistory (key) {
    const history = []
    for (const prevKey of this.getPrevious(key)) {
      if (!history.includes(prevKey)) {
        history.push(prevKey)
        for (const prev of this.getHistory(prevKey)) {
          if (!history.includes(prev)) history.push(prev)
        }
      }
    }
    return history
  }
}

function error (text) {
  throw new Error(`@tangle/graph expects ${text}`)
}

function containsSelfLoop (nodes) {
  return nodes.some((node) => (
    node.previous !== null &&
    node.previous.includes(node.key) // There is some node in nodes where node.key is in node.previous
  ))
}

/*
//Note that due to semiconnected nodes not appearing in graphs, we do not find larger Cycles

function containsCycles (connectedNodes, getBacklinks) {
  const seenNodes = new Set()
  const nodesToCheck = {}

  Object.values(connectedNodes).forEach(node => { // We will only look for connected cycles
    if (getBacklinks(node) === null) seenNodes.add(node.key)
    // Root nodes will not be part of a cycle
    else {
      nodesToCheck[node.key] = getBacklinks(node).filter(key => key in connectedNodes)
      // Create a dictionary of nodes to check which only contains connected nodes
    }
  })

  let foundLoop = false
  while (!foundLoop && Object.keys(nodesToCheck).length) {
    foundLoop = true
    Object.keys(nodesToCheck).forEach(nodeKey => {
      nodesToCheck[nodeKey] = nodesToCheck[nodeKey].filter(backlink => !seenNodes.has(backlink))
      // Remove all nodes NOT in cycle from 'previous'

      if (nodesToCheck[nodeKey].length === 0) {
        foundLoop = false
        seenNodes.add(nodeKey) // We have deleted all previous so we know it's not part of a cycle
        delete nodesToCheck[nodeKey]
      }
    })
  }
  return foundLoop
}
*/

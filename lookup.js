const isRoot = require('./lib/is-root')
// makes two dictionaries, one for connected nodes, one for disconnected
// each dictionary maps key -> node (where key is unique idenentifier for node)

// e.g.
//
//    A   (root)
//    |
//    B           J? (K points backto this but we don't have it)
//    |           |
//    C           K
//
// lookup = {
//   connected: { A, B, C },
//   disconnected: { J: null, K }
// }

module.exports = class Lookup {
  constructor (opts = {}) {
    let {
      nodes,
      linkMap,
      entryKeys // optional
    } = opts
    if (!entryKeys) {
      entryKeys = nodes
        .filter(node => isRoot(node))
        .map(node => node.key)
    }

    this.connected = {}
    this.disconnected = {}

    nodes.forEach(node => {
      this.disconnected[node.key] = node
    })
    // Copy the graph roots to start searching for connected nodes
    const queue = [...entryKeys]
    let key
    while (queue.length) {
      key = queue.pop()
      if (this.connected[key]) continue

      // Check if all previous are connected already
      if (this.disconnected[key].previous !== null) {
        const previousNodesConnected = this.disconnected[key].previous.every(previousKey => {
          return this.connected[previousKey]
        })
        // If not, add them to the holding cell (remains disconnected)
        if (!previousNodesConnected) continue
      }

      // If it is then
      // move record from 'disconnected' dict to 'connected' dict
      this.connected[key] = this.disconnected[key]
      delete this.disconnected[key]

      linkMap.get(key).forEach(linkedKey => queue.unshift(linkedKey))
    }

    // insert referenced but unknown nodes into disconnected
    // storing the value as null
    Object.values(this.disconnected).forEach(node => {
      const previous = node.previous
      if (previous === null) return // isRoot

      previous.forEach(linkedKey => {
        if (!this.disconnected[linkedKey] && !this.connected[linkedKey]) this.disconnected[linkedKey] = null
      })
    })
  }

  getNode (key) {
    if (this.connected[key]) return this.connected[key]
    else if (this.disconnected[key]) {
      console.warn(`key ${key} not in graph`)
      return null
    } else {
      console.warn(`key ${key} found, but is disconnected from main graph`)
      return undefined
    }
  }

  isConnected (key) {
    return Boolean(this.connected[key])
  }

  disconnect (keys) {
    keys.forEach(key => {
      if (!this.connected[key]) return

      this.disconnected[key] = this.connected[key]
      delete this.connected[key]
    })
  }
}

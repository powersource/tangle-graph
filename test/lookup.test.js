const test = require('tape')
const Lookup = require('../lookup')
const { LinkMap } = require('../maps')

const getBacklinks = node => node.previous

test('Lookup', t => {
  // ## message in-thread but "dangling" (doesn't link up to known messages)
  //
  //    A   (root)
  //    |
  //    B     ----?--- J? (a message we don't have)
  //    |              |
  //    C              K

  const A = { key: 'A', previous: null }
  const B = { key: 'B', previous: ['A'] }
  const C = { key: 'C', previous: ['B'] }
  const K = { key: 'K', previous: ['J'] } // disconnected

  const linkMap = new LinkMap([A, B, K, C], getBacklinks)

  const lookup = new Lookup({
    nodes: [A, B, C, K],
    // entryKeys: ['A'],
    linkMap
  })

  t.deepEqual(lookup.getNode('A'), A, 'getNode')
  t.equal(lookup.getNode('K'), null, 'getNode (disconnected key)')
  t.equal(lookup.getNode('Y'), undefined, 'getNode (invalid key)')

  t.deepEqual(lookup.connected, { A, B, C }, 'categorises connected')
  t.deepEqual(lookup.disconnected, { K, J: null }, 'categorises disconnected')

  /* prune! */
  lookup.disconnect(['C'])
  t.deepEqual(lookup.getNode('A'), A, 'getNode')
  t.equal(lookup.getNode('C'), null, 'getNode (disconnected key)')

  t.deepEqual(lookup.connected, { A, B }, 'categorises connected')
  t.deepEqual(lookup.disconnected, { C, K, J: null }, 'categorises disconnected')

  t.end()
})

test('Lookup semiconnected', t => {
  // ## message in-thread but "dangling" (doesn't link up to known messages)
  //
  //    A         K?
  //    |       /
  //    B     J
  //    |   /
  //    C

  const A = { key: 'A', previous: null }
  const B = { key: 'B', previous: ['A'] }
  const C = { key: 'C', previous: ['B', 'J'] }
  const J = { key: 'J', previous: ['K'] } // disconnected

  const linkMap = new LinkMap([A, B, C, J], getBacklinks)

  const lookup = new Lookup({
    nodes: [A, B, C, J],
    // entryKeys: ['A'],
    linkMap
  })

  t.deepEqual(lookup.getNode('A'), A, 'getNode')
  t.equal(lookup.getNode('C'), null, 'getNode (semiconnected key)')
  t.deepEqual(lookup.connected, { A, B }, 'categorises connected')
  t.deepEqual(lookup.disconnected, { C, J, K: null }, 'categorises disconnected')

  /* prune! */
  lookup.disconnect(['C']) // Disconnect a semiconnected key
  t.deepEqual(lookup.connected, { A, B }, 'categorises connected')
  t.deepEqual(lookup.disconnected, { C, J, K: null }, 'categorises disconnected')

  lookup.disconnect(['J']) // Disconnect a disconnected leading to semiconnected key
  t.deepEqual(lookup.connected, { A, B }, 'categorises connected')
  t.deepEqual(lookup.disconnected, { C, J, K: null }, 'categorises disconnected')

  t.end()
})

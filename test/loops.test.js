const test = require('tape')
const Graph = require('..')

test('Self loop', t => {
  const A1 = { key: 'A1', previous: null }
  const SL = { key: 'SL', previous: ['SL', 'A1'] } // Selfloop

  t.throws(() => new Graph([A1, SL]), /there is a selfloop in nodes/, 'new Selfloop Graph')

  const graph = new Graph([A1])
  t.throws(() => graph.addNodes([SL]), /there is a selfloop in nodes/, 'addNodes spots new selfloop')
  t.deepEqual(new Graph([A1]), graph, 'addNodes reverted if selfloop found')

  t.end()
})

test('Cycles (disconnected)', t => {
  //  C1---C2
  //   \  /
  //    C3

  //  C2---C3   A1
  //   \  /    /
  //    C1    /
  //      \  /
  //       C4

  const A1 = { key: 'A1', previous: null }

  const C1 = { key: 'C1', previous: ['C2'] }
  const C2 = { key: 'C2', previous: ['C3'] }
  const C3 = { key: 'C3', previous: ['C1'] }

  // One parent is the root but the other is part of a circle
  const C4 = { key: 'C4', previous: ['C1', 'A1'] }

  t.throws(() => new Graph([C1, C2, C3]), /at least one root node/, 'all graphs need some root')
  t.true(new Graph([C1, C2, C3, A1]), 'cycle that is not connected is fine')

  new Graph([C1, C2, C3, A1]).invalidateKeys(['C1']) // Can invalidate a node in a cycle if it's disconnected

  t.true(new Graph([C1, C2, C3, C4, A1]), 'new Circular Graph with merge node')

  const circularRootGraph = new Graph([C1, C2, C3, C4, A1])
  t.equal(circularRootGraph.isMergeNode('C4'), false, 'C4 is a semiconnected merge node')
  t.equal(circularRootGraph.isBranchNode('C1'), false, 'Circle C1 is disconnected')
  t.deepEqual(circularRootGraph.getLinks('C1'), [], 'getLinks of disconnected circle C1 gives []') // I expected this to give ['C4','C2'] since C1 is a branch node but it gives [] since it's disconnected
  t.deepEqual(circularRootGraph.getBacklinks('C1'), [], 'getBacklinks of circle C1') // I expected this to give ['C3']
  t.deepEqual(circularRootGraph.getBacklinks('C4'), [], 'C4 is semiconnected')
  console.log('circular root graph ', circularRootGraph.lookup.connected)

  t.deepEqual(circularRootGraph.getNode('C2'), null, 'node in circle not accessible')
  t.deepEqual(circularRootGraph.getNode('C4'), null, 'cant find C4 since it is semiconnected')

  t.end()
})

test('Cycles', t => {
  //   A1
  //   |
  //   V
  //   M1 <-- M3
  //   |     ^
  //   v    /
  //   M2 /

  const A1 = { key: 'A1', previous: null }

  const M1 = { key: 'M1', previous: ['A1', 'M3'] }
  const M2 = { key: 'M2', previous: ['M1'] }
  const M3 = { key: 'M3', previous: ['M2'] }

  const graph = new Graph([A1, M1, M2, M3])

  t.deepEqual(Object.keys(graph.lookup.connected), ['A1'], 'no cycle in list of connected nodes')

  // NOTE - these tests are failing because the nodes are "semi-connected"
  // i.e. M1 is not considered connected because it's previous nodes don't all come from a root
  // so we never hit the cycle detector...

  // t.throws(() => new Graph([A1, M1, M2, M3]), /there is a cycle in connected nodes/, 'new Circular Graph')

  // t.throws(() => new Graph([A1, M1, M2]), /there is a cycle in connected nodes/, 'new Circular Graph')

  // const graph = new Graph([A1])
  // t.throws(() => graph.addNodes([M1, M2, M3]), /there is a cycle in connected nodes/, 'addNodes throws if it creates cycle')

  t.end()
})

test('Cycles (2)', t => {
  // Big mess connected to a root
  //  A1
  //   \\\
  //    M1, )
  //     ( M2, )
  //      (  M3

  const A1 = { key: 'A1', previous: null }

  const M1 = { key: 'M1', previous: ['A1', 'M2', 'M3'] }
  const M2 = { key: 'M2', previous: ['A1', 'M1', 'M3'] }
  const M3 = { key: 'M3', previous: ['A1', 'M1', 'M2'] }
  /*
  We check if there is a cycle in the loop.
  We do this by repeatedly removing nodes that are not part of a cycle, starting from the roots
  {
    A1: { key: 'A1', previous: null },
    M1: { key: 'M1', previous: [ 'A1', 'M2', 'M3' ] },
    M2: { key: 'M2', previous: [ 'A1', 'M1', 'M3' ] },
    M3: { key: 'M3', previous: [ 'A1', 'M1', 'M2' ] }
  }
  {
    M1: [ 'A1', 'M2', 'M3' ],
    M2: [ 'A1', 'M1', 'M3' ],
    M3: [ 'A1', 'M1', 'M2' ]
  }
  {
    M1: [ 'M2', 'M3' ],
    M2: [ 'M1', 'M3' ],
    M3: [ 'M1', 'M2' ]
  }
  When we can't remove any more we have either found a cycle (like above) or shown that none of the nodes are in a loop.

  Note, we only check connected nodes and initially remove any disconnected nodes from previous.
  */
  const graph = new Graph([A1, M1, M2, M3])
  t.deepEqual(Object.keys(graph.lookup.connected), ['A1'], 'no cycle in list of connected nodes')
  t.end()
})

test('Cycles (3)', t => {
  //    A1
  //   / \
  //  / _ \
  //  B1_B2

  const A1 = { key: 'A1', previous: null }

  const B1 = { key: 'B1', previous: ['A1', 'B2'] }
  const B2 = { key: 'B2', previous: ['A1', 'B1'] }

  const graph = new Graph([A1, B1, B2])
  // t.throws(() => new Graph([A1, B1, B2]), /there is a cycle in connected nodes/, 'new Circular Graph')

  t.equal(graph.isBranchNode('A1'), false, 'The root branches to a loop')
  t.equal(graph.isTipNode('A1'), true, 'The loop is semiconnected')

  t.end()
})

const test = require('tape')
const Graph = require('..')

test('Graph with nodes []', t => {
  const graph = new Graph([])
  t.true(graph, 'can create graph')

  t.deepEqual(graph.rootNodes, [], 'has no rootNodes')
  t.deepEqual(graph.rootNodeKeys, [], 'has no rootNodes')

  t.end()
})

//     A   (root)
//    / \
//   B   C        --W (disconnected)
//    \ /           |
//     D            X (disconnected)

const A = { key: 'A', previous: null }
const B = { key: 'B', previous: ['A'] }
const C = { key: 'C', previous: ['A'] }
const D = { key: 'D', previous: ['B', 'C'] }

const W = { key: 'W', previous: ['V'] } // disconnected
const X = { key: 'X', previous: ['W'] } // disconnected
// const Z                                                     // exterior

const graph = new Graph([A, B, C, D, W, X])

test('new Graph', t => {
  t.true(new Graph([A, B, C, D, W, X]), 'new Graph')

  t.throws(
    () => new Graph([A, B, B]),
    /each node to have a unique key/,
    'throws if non-unique node keys'
  )

  t.throws(
    () => new Graph([B, C]),
    /at least one root node/,
    'throws if no root node(s)'
  )

  t.end()
})

test('graph.rootKeys', t => {
  t.deepEqual(graph.rootKeys, ['A'], 'rootKeys')
  t.deepEqual(graph.rootNodeKeys, ['A'], 'alias: rootNodeKeys')

  t.end()
})

test('graph.rootNodes', t => {
  t.deepEqual(graph.rootNodes, [A], 'rootNodes')

  t.end()
})

test('graph.getNode', t => {
  t.equal(graph.getNode('A'), A, 'getNode')
  t.equal(graph.getNode('W'), null, 'disconnected node not returned')
  t.equal(graph.getNode('X'), null, 'disconnected node not returned')
  t.equal(graph.getNode('Z'), undefined, 'exterior node not return')

  t.end()
})

test('graph.getLinks', t => {
  t.deepEqual(graph.getNext('A'), ['B', 'C'], 'getLinks')
  t.deepEqual(graph.getNext('W'), [], 'no links for disconnected')
  t.deepEqual(graph.getNext('X'), [], 'no links for disconnected')
  t.deepEqual(graph.getNext('Z'), [], 'no links for exterior')

  t.end()
})

test('graph.getBacklinks', t => {
  /* getBacklinks */
  t.deepEqual(graph.getPrevious('B'), ['A'], 'getBacklinks')
  t.deepEqual(graph.getPrevious('W'), [], 'no backlinks for disconnected')
  t.deepEqual(graph.getPrevious('X'), [], 'no backlinks for disconnected')
  t.deepEqual(graph.getPrevious('Z'), [], 'no backlinks for exterior')

  t.end()
})

test('graph.isBranchNode', t => {
  /* isBranchNode */
  t.equal(graph.isBranchNode('A'), true, 'A is a branchNode')
  t.equal(graph.isBranchNode('B'), false, 'B not branchNode')
  t.equal(graph.isBranchNode('D'), false, 'D not branchNode')
  t.equal(graph.isBranchNode('W'), false, 'W not branchNode') // disconnected node
  t.equal(graph.isBranchNode('X'), false, 'X is not a branch node') // disconnected node
  t.equal(graph.isBranchNode('Z'), false, 'Z is not a branch node') // exterior node

  t.end()
})

test('graph.isMergeNode', t => {
  t.equal(graph.isMergeNode('A'), false, 'A not merge node')
  t.equal(graph.isMergeNode('B'), false, 'B not merge node')
  t.equal(graph.isMergeNode('D'), true, 'D is a merge node')
  t.equal(graph.isMergeNode('X'), false, 'X not merge node') // disconnected
  t.equal(graph.isMergeNode('Y'), false, 'Y not merge node') // disconnected node
  t.equal(graph.isMergeNode('Z'), false, 'Z not merge node') // exterior node

  t.end()
})

test('graph.isTipNode', t => {
  t.equal(graph.isTipNode('A'), false, 'A not headNode')
  t.equal(graph.isTipNode('B'), false, 'B not headNode')
  t.equal(graph.isTipNode('D'), true, 'D is headNode')
  t.equal(graph.isTipNode('W'), false, 'W not headNode') // disconnected
  t.equal(graph.isTipNode('X'), false, 'X not headNode') // disconnected
  t.equal(graph.isTipNode('Z'), false, 'Z not headNode') // exterior node

  t.end()
})

test('graph.invalidateKeys', t => {
  graph.invalidateKeys(['B'])
  //     A   (root)
  //      \
  //   B   C
  //
  //     D

  t.deepEqual(graph.getNode('B'), null, 'pruned node not accessible')
  t.deepEqual(graph.getNode('D'), null, 'pruned node children not accessible')
  t.deepEqual(graph.getNext('A'), ['C'], 'updated links after prune')
  t.deepEqual(graph.getNext('B'), [], 'updated links after prune')
  t.deepEqual(graph.getNext('C'), [], 'updated links after prune')

  t.end()
})

//   A1  A2   (roots)
//    \ /
//     J
const A1 = { key: 'A1', previous: null }
const A2 = { key: 'A2', previous: null }
const J = { key: 'J', previous: ['A1', 'A2'] }

test('Two roots', t => {
  t.true(new Graph([A1, J]), 'Missing one of its roots')
  t.deepEqual(new Graph([A1, A2, J]).rootNodes, [A1, A2], 'Graph can have multiple rootNodes')
  t.deepEqual(new Graph([A1, J]).getPrevious('J'), [], 'getPrevious for semiconnected')
  t.deepEqual(new Graph([A1, J]).isMergeNode('J'), false, 'missing node in graph so not a a mergeNode')

  t.end()
})

test('bigNode', t => {
  const node = {
    key: '%kK0dpjFTvDVsb0eYTEVuW2Anq1rXkx5C94/Gms6ja2Y=.sha256',
    value: {
      previous: null,
      sequence: 1,
      author: '@Yqd9kfpcIB/ePcLaI/CFYB+Pg4UnxgOS/c/6cDm+2I8=.ed25519',
      timestamp: 1606699262888,
      hash: 'sha256',
      content: {
        type: 'profile/person',
        authors: {
          '@Yqd9kfpcIB/ePcLaI/CFYB+Pg4UnxgOS/c/6cDm+2I8=.ed25519': {
            0: 1
          }
        },
        tangles: {
          profile: {
            root: null,
            previous: null
          }
        }
      },
      signature: 'YH29AfSw6bg0aVF4aekjG8zGFSASgb7i7Zrapfi+iQ706dOvp1dePB4+tehXYvrxieWsMFRNkEEsd75jzvB2Aw==.sig.ed25519'
    },
    timestamp: 1606699262889
  }
  t.throws(
    () => new Graph([node]),
    /expects at least one root node/,
    'without getBacklinks we cant find the rootnode'
  )
  // t.true(new Graph([node], { getBacklinks: m => m.value.content.tangles.profile.previous }), 'can create a graph with a big node')

  t.end()
})

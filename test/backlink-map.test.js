const test = require('tape')
const { BacklinkMap } = require('../maps')

test('BacklinkMap', t => {
  //     A   (root)
  //    / \
  //   B   C
  //    \ /
  //     D

  const map = {
    A: { B: 1, C: 1 },
    B: { D: 1 },
    C: { D: 1 }
  }

  const expectedRevserseMap = {
    D: { B: 1, C: 1 },
    C: { A: 1 },
    B: { A: 1 }
  }

  // NOTE BacklinkMap takes LinkMap.raw || LinkMap
  t.deepEqual(new BacklinkMap(map).raw, expectedRevserseMap, 'happy')
  t.end()
})

const LinkMap = require('./link-map')
const BacklinkMap = require('./backlink-map')

// map objects which represent forward and backward linking maps of the graph
// e.g.
//
//     A   (root)
//    / \
//   B   C
//    \ /
//     D
//
// linkMap = {
//   A: { B: 1, C: 1 },
//   B: { D: 1 },
//   C: { D: 1 }
// }
//
// backlinkMap = {
//   D: { B: 1, C: 1 },
//   B: { A: 1 },
//   C: { A: 1 }
// }

module.exports = {
  LinkMap,
  BacklinkMap
}

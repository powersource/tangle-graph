const set = require('lodash.set')
const BaseMap = require('./base-map')
const LinkMap = require('./link-map')

module.exports = class BacklinkMap extends BaseMap {
  constructor (map) {
    super()

    const source = (map.constructor === LinkMap) ? map.raw : map

    forEach(source, ([fromNode, toNodes]) => {
      forEach(toNodes, ([toNode, distance]) => {
        set(this.map, [toNode, fromNode], distance)
      })
    })
  }
}

function forEach (obj, cb) {
  Object.entries(obj).forEach(cb)
}
